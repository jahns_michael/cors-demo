var express = require('express')
var app = express()

app.set('view engine', 'ejs')

app.get('/', function (req, res, next) {
  res.render('index', {port: process.argv[2]});
})

app.listen(process.argv[2], function () {
  console.log(`Origin : ${process.argv[2]}`)
})
