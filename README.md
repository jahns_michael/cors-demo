Repository ini berisikan berkas-berkas yang dipakai untuk mendemokan cara kerja library _cors_ pada aplikasi server Node JS.

# Tata cara demo

1. Clone repo ini
2. Install dependency

```bash
npm install
```

3. Jalankan server, server akan berjalan pada PORT 3000

```bash
node server.js
```

4. Jalankan client, anda dapat mengganti `<PORT>` dengan 5000 atau 8000 atau apapun. Terdapat endpoint khusus untuk origin 5000 dan 8000, namun tidak untuk yang lain

```bash
node client.js PORT

contoh: node client.js 5000
```

5. Buka `https://localhost:<PORT>`, misalnya `https://localhost:5000`, sesuai dengan di port mana anda menjalankan client.js sebelumnya.

6. Silakan click button yang tersedia dan lihat hasilnya, bandingkan dengan endpoint lainnya.
