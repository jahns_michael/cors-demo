var express = require('express')
var cors = require('cors')
var app = express()

var cors5000 = {
  origin: 'http://localhost:5000'
}

var cors8000 = {
  origin: 'http://localhost:8000'
}

app.get('/cors5000', cors(cors5000), function (req, res, next) {
  console.log(req.headers.origin);
  res.json({msg: 'This is CORS-enabled for http://localhost:5000 only'})
})
app.get('/cors8000', cors(cors8000), function (req, res, next) {
  console.log(req.headers.origin);
  res.json({msg: 'This is CORS-enabled for http://localhost:8000 only'})
})

app.get('/', cors(), function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origin'})
})

app.listen(3000, function () {
  console.log('CORS-enabled web server listening on port 3000')
})
